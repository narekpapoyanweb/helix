<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeoNamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('geo_names', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->unsignedInteger( 'geonameid' );
            $table->string('name', 200)->nullable();
            $table->string('asciiname', 200)->nullable();
            $table->string('alternatenames', 10000)->nullable();
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->string('feature_class', 1)->nullable();
            $table->string('feature_code', 10)->nullable();
            $table->string('country_code', 2)->nullable();
            $table->string('cc2', 200)->nullable();
            $table->string('admin1_code', 20)->nullable();
            $table->string('admin2_code', 80)->nullable();
            $table->string('admin3_code', 20)->nullable();
            $table->string('admin4_code', 20)->nullable();
            $table->unsignedBigInteger( 'population' )->nullable();
            $table->integer('elevation')->nullable();
            $table->unsignedInteger( 'dem' )->nullable();
            $table->string('timezone', 40)->nullable();
            $table->date('modification_date')->nullable();
            $table->timestamps();

            $table->primary('geonameid');
            $table->index('name');
            $table->index('asciiname');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('geo_names');
    }
}
