
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
require('./jquery-ui');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key)))

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
    el: '#app'
});
$.ajaxSetup({
    headers:
        { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') }
});

let search_source = $("#search").attr('action');
let nearest_source = $("#search").data('nearest-source');

$("#city").autocomplete({
    source: function( request, response ) {
        $.ajax( {
            url: search_source,
            data: {
                city: request.term
            },
            success: function( data ) {
                response( data );
            }
        } );
    },
    select: function( event, ui ) {
        $.ajax( {
            url: nearest_source,
            data: {
                id: ui.item.id
            },
            success: function( data ) {
                initMap(data);
            }
        } );
    }
});

function initMap(data) {

    let map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: data['main']['location'],
        mapTypeId: 'satellite'
    });

    // Add some markers to the map.
    // Note: The code uses the JavaScript Array.prototype.map() method to
    // create an array of markers based on a given "locations" array.
    // The map() method here has nothing to do with the Google Maps API.
    let markers = data['nearests'].map(function(location, i) {
        return new google.maps.Marker({
            position: location['location'],
            label: location['name']
        });
    });
    markers.push(new google.maps.Marker({
        position: data['main']['location'],
        label: data['main']['name']
        })
    );

    // Add a marker clusterer to manage the markers.
    let markerCluster = new MarkerClusterer(map, markers,
        {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});
}
