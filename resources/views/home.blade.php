@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-header">Hello</div>
        <div class="ui-widget card-body">
            <form id="search" action="{{ route('api.geo.search') }}" data-nearest-source="{{ route('api.geo.nearest') }}">
                <div class="row">
                    <div class="col-sm-8">
                        <div class="form-group">
                            <label for="city" class="col-form-label">City</label>
                            <input id="city" class="form-control" name="city" value="">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div id="map"></div>


@endsection

@section('scripts')
    <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDM3jTTaM06GQdg0MvSXBhLDLGXM2QOHdw&callback=initMap"></script>
@endsection