<?php

namespace App\UseCases;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;


class GeonamesService
{
    const URL = 'http://download.geonames.org/export/dump';
    const ETAG = 'Etag.txt';

    private $table_name;
    private $local_file_dir;
    private $country_iso_code;
    private $source_url;
    private $localTxtFilePath;
    private $localZipFilePath;
    private $etagFilePath;

    public function __construct(string $table_name, string $local_file_dir, string $country_iso_code) {

        $this->table_name = $table_name;
        $this->local_file_dir = $local_file_dir;
        $this->country_iso_code = $country_iso_code;
        $this->source_url = self::URL.'/'.$country_iso_code.'.zip';
        $this->localZipFilePath = $local_file_dir.DIRECTORY_SEPARATOR.$country_iso_code.'.zip';
        $this->localTxtFilePath = $local_file_dir.DIRECTORY_SEPARATOR.$country_iso_code.'.txt';
        $this->etagFilePath = $local_file_dir.DIRECTORY_SEPARATOR.'Etag.txt';
    }

    public function download()
    {
        $client = new Client();
        $response = $client->request('GET', $this->source_url);

        Storage::put($this->localZipFilePath, $response->getBody());
        Storage::put($this->etagFilePath, $response->getHeader('Etag'));

        $zip = new \ZipArchive();
        $zipOpenResult = $zip->open( Storage::path($this->localZipFilePath) );
        if ( TRUE !== $zipOpenResult ) {
            throw new \DomainException( "Error [" . $zipOpenResult . "] Unable to unzip the archive at " . $this->localZipFilePath );
        }
        $extractResult = $zip->extractTo( Storage::path($this->local_file_dir) );
        if ( FALSE === $extractResult ) {
            throw new \DomainException( "Unable to unzip the file at " . $this->local_file_dir );
        }
        $closeResult = $zip->close();
        if ( FALSE === $closeResult ) {
            throw new \DomainException( "After unzipping unable to close the file at " . $this->localZipFilePath );
        }
    }

    public function check()
    {
        $client = new Client();
        $response = $client->get($this->source_url, [
            'headers' => [
                'If-None-Match' => $this->getLastEtag(),
            ],
        ]);

        if ($response->getStatusCode() === 304) {
            return false;
        } elseif ($response->getStatusCode() === 200 && $response->getBody()->getSize() > 0) {
            return true;
        }
    }

    public function cleanDir()
    {
        Storage::deleteDirectory($this->local_file_dir);
    }

    public function insertData()
    {
        if ( ! file_exists( Storage::path($this->localTxtFilePath) ) ) {
            throw new \DomainException( "Cannot find file: ".$this->localTxtFilePath );
        }
        $query = "LOAD DATA LOCAL INFILE '" . Storage::path($this->localTxtFilePath) . "'
            INTO TABLE " . $this->table_name . " CHARACTER SET utf8mb4
            (geonameid, 
             name, 
             asciiname, 
             alternatenames, 
             latitude, 
             longitude, 
             feature_class, 
             feature_code, 
             country_code, 
             cc2, 
             admin1_code, 
             admin2_code, 
             admin3_code, 
             admin4_code, 
             population, 
             elevation, 
             dem, 
             timezone, 
             modification_date, 
             @created_at, 
             @updated_at)
        SET created_at=NOW(),updated_at=null";


        if ( !DB::getPdo()->exec( $query ) ) {
            throw new \DomainException( "Unable to insert data . ". print_r( DB::getPdo()->errorInfo(), TRUE ) );
        }
    }

    public function getLastEtag()
    {
        if( !file_exists(Storage::path($this->etagFilePath)) ){
            throw new \DomainException( "Cannot find the file . ". $this->etagFilePath." Note: at first you must insert data with 'php artisan geo:insert' console command!" );
        }
        $line = file_get_contents(Storage::path($this->etagFilePath));

        if($line === false){
            throw new \DomainException( "Unable to read the file . ". $this->etagFilePath );
        }
        return $line;
    }

    public function truncate(): void
    {
        DB::table($this->table_name)->truncate();
    }
}