<?php

namespace App\Http\Controllers;


use App\Entity\GeoName;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    public function index()
    {
        return view('home');
    }
}
