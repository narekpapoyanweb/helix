<?php

namespace App\Http\Controllers\Api;

use App\Entity\GeoName;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GeoController extends Controller
{
    public function __construct()
    {
    }

    public function search(Request $request)
    {
        $term = $request->get('city');
        $cities = Geoname::orderByDesc('name')->select('geonameid','name')->where('name', 'like',$term . '%')->limit(100)->get();

        $data=[];
        foreach($cities as $city){
            $data[] = ['id'=> $city->geonameid,'value' => $city->name ];
        }
        return $data;
    }

    public function nearest(Request $request)
    {
         $id = $request->get('id');
        $citiy = Geoname::find($id);
        $data['main']['name'] = $citiy->name;
        $data['main']['location'] = (object)['lng'=>(float)$citiy->longitude,'lat'=>(float)$citiy->latitude];

        // get latitude and longitude from geocode object
        $latitude = $citiy->latitude;
        $longitude = $citiy->longitude;

        // set request options
        $responseStyle = 'short'; // the length of the response
        $radius = 300; // the radius in KM
        $maxRows = 20; // the maximum number of rows to retrieve
        $username = 'narekpapoyan'; // the username of your GeoNames account

        // get nearby cities based on range as array from The GeoNames API
        $nearestCities = json_decode(file_get_contents('http://api.geonames.org/findNearbyPlaceNameJSON?lat='.$latitude.'&lng='.$longitude.'&style='.$responseStyle.'&radius='.$radius.'&maxRows='.$maxRows.'&username='.$username, true));
        foreach($nearestCities->geonames as $geoname){
            $data['nearests'][] = ['name'=> $geoname->name, 'location'=> (object)['lng'=>(float)$geoname->lng,'lat'=>(float)$geoname->lat] ];
        }
        return $data;
    }

}