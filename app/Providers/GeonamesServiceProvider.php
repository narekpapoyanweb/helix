<?php

namespace App\Providers;

use App\UseCases\GeonamesService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;

class GeonamesServiceProvider extends ServiceProvider
{
    public function register(): void
    {
        $this->app->singleton(GeonamesService::class, function (Application $app) {
            $config = $app->make('config')->get('geonames');

            return new GeonamesService($config['table_name'], $config['local_file_dir'], $config['country_iso_code']);
        });
    }
}