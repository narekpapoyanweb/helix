<?php
namespace App\Console\Commands;


use App\UseCases\GeonamesService;
use Illuminate\Console\Command;

class InsertCommand extends Command
{
    protected $signature = 'geo:insert';

    private $geonames;

    public function __construct(GeonamesService $geonames)
    {
        parent::__construct();
        $this->geonames = $geonames;
    }

    public function handle(): bool
    {
        $success = true;
        try {

            $this->info('Downloading ZIP files.');
            $this->geonames->download();
            $this->info('ZIP files are downloaded and unzipped.');

            $this->info('Truncate old data in database.');
            $this->geonames->truncate();

            $this->info('Inserting data from file.');
            $this->geonames->insertData();
            $this->info('Geonames data has been inserted in database.');

        } catch (\DomainException $e) {
            $this->error($e->getMessage());
            $success = false;
        }


        return $success;
    }
}