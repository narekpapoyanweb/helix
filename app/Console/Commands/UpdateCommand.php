<?php
namespace App\Console\Commands;


use App\UseCases\GeonamesService;
use Illuminate\Console\Command;

class UpdateCommand extends Command
{
    protected $signature = 'geo:update';

    private $geonames;

    public function __construct(GeonamesService $geonames)
    {
        parent::__construct();
        $this->geonames = $geonames;
    }

    public function handle(): bool
    {
        $success = true;
        try {
            if( $this->geonames->check() ){

                $this->info('Deleting old files.');
                $this->geonames->cleanDir();

                $this->info('Downloading ZIP files.');
                $this->geonames->truncate();
                $this->info('ZIP files are downloaded and unzipped.');

                $this->info('Truncate old data in database.');
                $this->geonames->clear();

                $this->info('Inserting data from file.');
                $this->geonames->insertData();
                $this->info('Geonames data has been inserted in database.');
            }
            $this->info('The resource has not been updated.');


        } catch (\DomainException $e) {
            $this->error($e->getMessage());
            $success = false;
        }


        return $success;
    }
}