<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $geonameid
 * @property string $name
 * @property string $asciiname
 * @property string $alternatenames
 * @property int|null $latitude
 * @property int|null $longitude
 * @property string $feature_class
 * @property string $feature_code
 * @property string $country_code
 * @property string $cc2
 * @property string $admin1_code
 * @property string $admin2_code
 * @property string $admin3_code
 * @property string $admin4_code
 * @property int|null $population
 * @property int|null $elevation
 * @property int|null $dem
 * @property string $timezone
 */
class GeoName extends Model
{
    protected $table = 'geo_names';
    protected $primaryKey = 'geonameid';

    protected $guarded = [];
}
