<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Data file path relative to the storage
    |--------------------------------------------------------------------------
    |
    */
    'local_file_dir' => env('GEO_LOCAL_FILE_DIR', 'geo'),
    'table_name' => env('GEO_TABLE_NAME', 'geo_names'),
    'country_iso_code' => env('GEO_COUNTRY_ISO_CODE', 'RU'), // for http://download.geonames.org/export/dump/ -- Ru.zip
];
